//
//  ViewController.swift
//  TF Messenger
//
//  Created by Поднебесные on 20.09.2018.
//  Copyright © 2018 Undersky. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var addImageButton: UIButton!
    @IBOutlet weak var accountImage: UIImageView!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var closeButton: UIBarButtonItem!
    

    /*
     func init() {
        print("Frame edit button (init): \(editButton.frame)")
        // пытаемся распечатать до загрузки вью
    }
     */
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addImageButton.layer.cornerRadius = self.addImageButton.bounds.size.width / 2
        self.addImageButton.layer.masksToBounds = true

        self.accountImage.layer.cornerRadius = self.accountImage.bounds.size.width / 6
        self.accountImage.layer.masksToBounds = true
        
        self.editButton.layer.cornerRadius = self.editButton.bounds.size.width / 22
        self.editButton.layer.borderWidth = 1.5
        self.editButton.layer.borderColor = UIColor.black.cgColor
        self.editButton.layer.masksToBounds = true
        print("Frame editbutton (\(#function)): \(editButton.frame)")
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        print("Frame editbutton (\(#function)): \(editButton.frame)")
        
        //AutoLayout не меняет размеры нашего вида сразу в методе viewDidLoad. Меняется в зависимости от выбранного устройства (Например, если в сториборде выбран 5s и в качестве симулятора 5s, то значения будут одинаковы)
    
    }

    
    @IBAction func chooseImageButton(_ sender: Any) {
        
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        
        print("Выбери изображение профиля")
        
        let actionSheet = UIAlertController(title: "Хотите новый аватар?", message: "Сделайте снимок или выберите фото из библиотеки", preferredStyle: UIAlertControllerStyle.actionSheet)
           
        actionSheet.addAction(UIAlertAction(title: "Сделать снимок", style: .default, handler: { (action: UIAlertAction) in
                if UIImagePickerController.isSourceTypeAvailable(.camera) {
                    imagePickerController.sourceType = .camera
                    imagePickerController.showsCameraControls = true
                    imagePickerController.delegate = self
                    self.present(imagePickerController, animated: true, completion: nil)
                } else {
                    print("Camera is not available")
                    let alert  = UIAlertController(title: "Ого!", message: "А камеры то и нет", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "ОК :(", style: .default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }))
        
        actionSheet.addAction(UIAlertAction(title: "Выбрать фото", style: .default, handler: { (action: UIAlertAction) in
            imagePickerController.sourceType = .photoLibrary
            imagePickerController.delegate = self
            imagePickerController.allowsEditing = true
            self.present(imagePickerController, animated: true, completion: nil)
        
        }))
        
        actionSheet.addAction(UIAlertAction.init(title: "Отмена", style: .cancel, handler: nil))
        self.present(actionSheet, animated: true, completion: nil)
    }
    
func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {

    let imageFromCamera = info[UIImagePickerControllerOriginalImage] as! UIImage
    accountImage.image = imageFromCamera
    picker.dismiss(animated:true, completion:nil)
    }
    
func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)    }
 
    @IBAction func backButt(_ sender: Any) {

            self.dismiss(animated: true, completion: nil)
    }
    
    
    
}


    
    
    
    





