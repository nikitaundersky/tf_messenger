//
//  ChatProtocol.swift
//  TF Messenger
//
//  Created by Поднебесные on 06.10.2018.
//  Copyright © 2018 Undersky. All rights reserved.
//

import Foundation

protocol ChatProtokol : class {
    var name : String? {get set}
    var message : String? {get set}
    var date : String? {get set}
    var online : Bool {get set}
    var hasUnreadMessage : Bool {get set}
    
    
    
}



protocol MessageSwiftModel: class {
    var textMess : String? {get set}
}

