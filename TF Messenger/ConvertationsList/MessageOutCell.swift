//
//  MessageOutCell.swift
//  TF Messenger
//
//  Created by Поднебесные on 07.10.2018.
//  Copyright © 2018 Undersky. All rights reserved.
//

import UIKit

class MessageOutCell: UITableViewCell, MessageSwiftModel {
    
    var textMess: String? {
        get { return messageOutLabel.text}
        set { messageOutLabel.text = newValue}
    }
    

    @IBOutlet weak var messageOutLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
   
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

 
    }
    
}
