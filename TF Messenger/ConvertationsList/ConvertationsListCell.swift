//
//  ConvertationsListCell.swift
//  TF Messenger
//
//  Created by Поднебесные on 04.10.2018.
//  Copyright © 2018 Undersky. All rights reserved.
//

import UIKit

class ConvertationsListCell: UITableViewCell, ChatProtokol {


    
    var name: String? {
        get { return titleLabel.text}
        set { titleLabel.text = newValue}
    }

    var message: String? {
        get { return messageLabel.text}
        set { messageLabel.text = newValue}
    }
    
    var date: String? {
        get { return dateLabel.text}
        set { dateLabel.text = newValue}
    }
    var online: Bool {
        get { return backgroundColor != UIColor.white }
        set { if newValue {
            backgroundColor = UIColor(red: 255.0/255.0, green:255.0/255.0, blue:220.0/255.0, alpha: 1.0)
        } else {
            backgroundColor = UIColor.white
            }
        }
    }
    
    var hasUnreadMessage: Bool {
        get {return messageLabel.font != UIFont.systemFont(ofSize: 14)}
        set { if newValue {
            messageLabel.font = UIFont.boldSystemFont(ofSize: 14)
        } else {
            messageLabel.font = UIFont.systemFont(ofSize: 14)
            }
            
        }
    }
    


    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    
 
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
