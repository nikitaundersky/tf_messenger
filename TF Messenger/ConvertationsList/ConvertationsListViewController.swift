//
//  ConvertationsListViewController.swift
//  TF Messenger
//
//  Created by Поднебесные on 04.10.2018.
//  Copyright © 2018 Undersky. All rights reserved.
//

import UIKit

class ConvertationsListViewController: UIViewController, UINavigationControllerDelegate{
    


    
    
    @IBOutlet weak var tableView: UITableView!
    
    private let identifier = String(describing: ConvertationsListCell.self)
    let sections = ["Online", "History"]
    let names = [["Игорь", "Василий", "Никита", "Николай", "Олег", "Глеб", "Вова", "Ольга", "Алиса", "Валерий"],
                 ["Пётр", "Костя", "Михаил", "Вера", "Надежда", "Виктория", "Люба", "Антон", "Елена Васильевна", "Катюха"]]
    let messages = ["", "Понял", "Окей", "Так и сделаем", "До скорого", "Неа", "Давай в пятницу", "ммм", "Договорились", "Да"]
    let timeMess = ["21:25", "11:11", "22:11", "16:27", "15:51", "16:20", "22:55", "9:43", "17:20", "21:11"]
    let online = [true, false, false, false, true, false, true, false, true, true]
    let hasUnreadMessage = [true, false, true, true, true, false, false, false, true, true]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

        tableView.register(UINib(nibName: identifier, bundle: nil), forCellReuseIdentifier: identifier)
        tableView.dataSource = self
        tableView.delegate = self
   
    }
    
    @IBAction func profileButton(_ sender: Any) {
        if let vc = UIStoryboard(name: "Main", bundle: nil).instantiateInitialViewController() {
            self.present(vc, animated: true, completion: nil)
        }
    }


}

//MARK: - UITableViewDataSource
extension ConvertationsListViewController : UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.names[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as?
            ConvertationsListCell else {
                return UITableViewCell()
        }
        let name = self.names[indexPath.section][indexPath.row]
        cell.name = name

        let date = self.timeMess[indexPath.row]
        cell.date = date
        
        if self.messages[indexPath.row].isEmpty {
            cell.message = "No messages yet"
        } else {
           cell.message = self.messages[indexPath.row]
        }
        
        cell.online = self.online[indexPath.row]
        cell.hasUnreadMessage = self.hasUnreadMessage[indexPath.row]
        
        

    
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return self.sections[section]
    }
    
    
}

// MARK: - UITableViewDelegate

extension ConvertationsListViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let name = names[indexPath.section][indexPath.row]
        tableView.deselectRow(at: indexPath, animated: true)
        
        let storyBoard: UIStoryboard = UIStoryboard(name: "ConvertationsListViewController", bundle: nil)
        let MessagingViewController = storyBoard.instantiateViewController(withIdentifier: "MessagingViewController") as! MessagingViewController
        MessagingViewController.navigationItem.title = name
        self.navigationController?.pushViewController(MessagingViewController, animated: true)
        }
        
    }
    








