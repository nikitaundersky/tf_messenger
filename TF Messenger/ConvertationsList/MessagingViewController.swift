//
//  MessagingViewController.swift
//  TF Messenger
//
//  Created by Поднебесные on 06.10.2018.
//  Copyright © 2018 Undersky. All rights reserved.
//

import UIKit

class MessagingViewController: UIViewController, UINavigationControllerDelegate {
    
    var indexPath:IndexPath!

    
    
   
    
    @IBOutlet weak var messageTableView: UITableView!
    private let identifierIn = String(describing: MessageInCell.self)
    private let identifierOut = String(describing: MessageOutCell.self)
    
     let dialog = ["!", "?", "А здесь ровно 30 символов!! :)", "Ого Так много! Я тоже так умею",
                   "А здесь я возьму рыбный текст. Данный параметр показывает количество слов, состоящих из букв различных алфавитов. Часто это буквы русского и английского языка, например, слово «стол», где «о» - буква английского алфавита. Некоторые копирайтеры заменяют в русских словах часть букв на английские, чтобы!",
                   "Да и я тоже! Возможность нахождения поисковых ключей в тексте и определения их количества полезна как для написания нового текста, так и для оптимизации уже существующего. Расположение ключевых слов по группам и по частоте сделает навигацию по ключам удобной и быстрой. Сервис также найдет морфолога!"]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
   
        messageTableView.register(UINib(nibName: identifierIn, bundle: nil), forCellReuseIdentifier: identifierIn)
        messageTableView.register(UINib(nibName: identifierOut, bundle: nil), forCellReuseIdentifier: identifierOut)
        messageTableView.dataSource = self
        
        messageTableView.rowHeight = UITableViewAutomaticDimension
        messageTableView.estimatedRowHeight = 100

    }

}

extension MessagingViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dialog.count
    }
    

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row % 2 == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: identifierIn, for: indexPath) as! MessageInCell
            let dialog = self.dialog[indexPath.row]
            cell.textMess = dialog
            return cell
        } else {
            let cell2 = tableView.dequeueReusableCell(withIdentifier: identifierOut, for: indexPath) as! MessageOutCell
            let dialog = self.dialog[indexPath.row]
            cell2.textMess = dialog
            return cell2
        }
        

        

        
        
    }
    
    
    
    
    
}

//коммент для проверки git

