//
//  MessageInCell.swift
//  TF Messenger
//
//  Created by Поднебесные on 07.10.2018.
//  Copyright © 2018 Undersky. All rights reserved.
//

import UIKit

class MessageInCell: UITableViewCell, MessageSwiftModel {
    
    var textMess: String? {
        get { return messageInLabel.text}
        set { messageInLabel.text = newValue}
    }
   
    
    @IBOutlet weak var messageInLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    
    }

   
    
}
